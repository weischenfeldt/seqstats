########
# This R scripts generates a PDF with numerous sam/bam file statistics
# It needs input from samstatM, stats, isize, and cov
# (c) 2012 Thomas Zichner (thomas.zichner@embl.de) and Tobias Rausch
########

library(gplots)


# Reading arguments
args <- commandArgs(trailingOnly=T)
sampleFile <- args[1]
samstatDataFile <- args[2]
mappingStatNumFile <- args[3]
insertFile <- args[4]
coverageFile <- args[5]
gcContentFile <- args[6]
outputFile <- args[7]


# Setting variables
mapColors <- c("darkgreen", "red")
barColors <- c("gray", "gray", "#0000DD", "#AAAAFF", "darkgreen", "#55CC55")
catColors <- c("darkgreen","green","yellow","orange","red","darkgray")
nucColors <- c("#008000","#0000FF","#FFFF00","#FF0000","#808080")
nucleotids <- c("A","C","G","T","N")


# Reading results from stat output
mappingStatData <- read.delim(mappingStatNumFile, header=F, as.is=T)
genomeSize <- as.numeric(mappingStatData[3,2])
if (genomeSize==3095693983) { genomeSize <- 2861332606 } # http://www.ncbi.nlm.nih.gov/projects/genome/assembly/grc/human/data/index.shtml (ungapped placed scaffolds)
numTotalReads <- as.numeric(mappingStatData[5,2])
numReadsMapped <- as.numeric(mappingStatData[6,2])
numReadsUnMapped <- as.numeric(mappingStatData[8,2])
numTotalReads1 <- as.numeric(mappingStatData[10,2])
numReads1Mapped <- as.numeric(mappingStatData[11,2])
numReads1UnMapped <- as.numeric(mappingStatData[13,2])
numTotalReads2 <- as.numeric(mappingStatData[15,2])
numReads2Mapped <- as.numeric(mappingStatData[16,2])
numReads2UnMapped <- as.numeric(mappingStatData[18,2])
medianReadLength <- as.numeric(mappingStatData[21,2])
numReadsInPair <- as.numeric(mappingStatData[25,2])
numReadsInPairMapped <- as.numeric(mappingStatData[26,2])
numReadsInPairMappedNonRed <- as.numeric(mappingStatData[30,2])
numReadsInPairMappedSameChr <- as.numeric(mappingStatData[28,2])
numReadsInPairMappedSameChrNonRed <- as.numeric(mappingStatData[32,2])


# Reading results from samstat ouput and computing average values
samstatData <- read.delim(samstatDataFile, header=F, row.names=1, as.is=T)
numReads <- samstatData[1,1:6]
numReadsR1 <- samstatData[2,1:6]
numReadsR2 <- samstatData[3,1:6]

qualR1 <- samstatData[10:15,]
qualR2 <- samstatData[16:21,]
avgQualR1 <- apply(qualR1 * t(numReadsR1), 2, sum) / sum(numReadsR1)
avgQualR2 <- apply(qualR2 * t(numReadsR2), 2, sum) / sum(numReadsR2)

baseComp <- samstatData[22:51,]
baseCompR1 <- samstatData[52:81,]
baseCompR2 <- samstatData[82:111,]
avgBaseCompR1 <- samstatData[52:56,]
avgBaseCompR2 <- samstatData[82:86,]
for (i in 1:5) {
	avgBaseCompR1[i,] <- apply(baseCompR1[seq(i,30,5),] * t(numReadsR1), 2, sum) / sum(numReadsR1)
	avgBaseCompR2[i,] <- apply(baseCompR2[seq(i,30,5),] * t(numReadsR2), 2, sum) / sum(numReadsR2)
}

if (nrow(samstatData) < 156) {
	noAlignInfo <- TRUE
} else {
	noAlignInfo <- FALSE
}
mismatches <- samstatData[112:131,]
insertions <- samstatData[132:151,]
deletions <- samstatData[152:156,]
avgMismatches <- mismatches[1:4,]
avgInsertions <- insertions[1:4,]
avgDeletions <- apply(deletions * t(numReads), 2, sum) / sum(numReads)
for (i in 1:4) {
	avgMismatches[i,] <- apply(mismatches[seq(i,20,4),] * t(numReads[1:5]), 2, sum) / sum(numReads[1:5])
	avgInsertions[i,] <- apply(insertions[seq(i,20,4),] * t(numReads[1:5]), 2, sum) / sum(numReads[1:5])
}


# Reading gc content information as well as coverage
gc <- read.delim(gcContentFile, as.is=T, header=F)
cov <- read.delim(coverageFile, as.is=T, header=F)


# Reading insert sizes
d = read.delim(insertFile, header=F, as.is=T, colClasses="double", nrows=1000000); 
x = d[,1]
medianInsertSize <- median(x)
if (medianInsertSize > 2000) {
	medianInsertSize <- median(x[x>1000])
}
medianInsertSize <- as.integer(medianInsertSize)


########################
#### Start of plots ####
########################


pdf(outputFile, width=11, height=15)
layout(matrix(c(1,1,1,1,1,2,2,2,2,2,3,3,4,4,4,5,6,6,6,7,8,9,9,9,10,11,11,11,12,12,13,13,14,14,14,15,15,15,15,15), 8, 5, byrow = TRUE), widths=c(1.2,1.8,1.4,0.4,1.2), heights=c(0.3,0.1,0.7,2,0.6,2,2,1.3))

# Summary information
par(mar=c(0,0,0,0))
textplot(sampleFile, cex=2.5, mar=c(0,0,0,0), fixed.width=F, font=2)
textplot(date(), cex=1.5, mar=c(0,0,0,0), fixed.width=F)

data0_1 <- matrix(c(numTotalReads,numReadsMapped,numReadsInPairMapped,numReadsInPairMappedSameChrNonRed, "", sprintf("(%.2f %%)", c(numReadsMapped,numReadsInPairMapped,numReadsInPairMappedSameChrNonRed) / numTotalReads * 100), "", sprintf("%.2fx", c(numReadsMapped,numReadsInPairMapped,numReadsInPairMappedSameChrNonRed) * medianReadLength / genomeSize), "", "", sprintf("%.2fx", c(numReadsInPairMappedSameChr,numReadsInPairMappedSameChrNonRed) * medianInsertSize / genomeSize / 2)), 4, 4, byrow=F)
rownames(data0_1) <- c("Total", "Mapped", "Both mapped", "Same chr non-red")
colnames(data0_1) <- c("", "", "", "")
textplot(data0_1, cex=1.8, cmar=2, mar=c(0,0,1,0))

data0_2 <- matrix(c(sprintf("%5.f", genomeSize), paste(sprintf("%d", medianReadLength), sprintf("%d", medianInsertSize), sep=" / "), sprintf("%.2f %%", (1-(numReadsInPairMappedNonRed/numReadsInPairMapped))*100), sprintf("%.2f %%", numReadsInPairMappedSameChr / numReadsInPairMapped * 100)), 4, 1, byrow=F)
rownames(data0_2) <- c("Genome size", "Read / insert size", "Redundancy rate", "Same chr")
colnames(data0_2) <- c("")
textplot(data0_2, cex=1.8, cmar=2, mar=c(0,0,1,0))


# Bar charts
par(mar=c(3,3,3,1))
barplotData <- c(numReadsMapped/numTotalReads, numReads1Mapped/numTotalReads1, numReads2Mapped/numTotalReads2)
barplot(barplotData, col=mapColors[1], names=paste(sprintf("%.1f", as.vector(barplotData)*100), "%"), las=2, space=c(.5), ylim=c(0,1), cex.axis=1.0, cex.names=1.0, cex.main=1, main="% mapped reads (both,left,right)")
abline(h=seq(0,1,0.1), col="gray", lty=3)
barplot(barplotData, col=mapColors[1], names=paste(sprintf("%.1f", as.vector(barplotData)*100), "%"), las=2, space=c(.5), ylim=c(0,1), cex.axis=1.0, cex.names=1.0, add=T)

barplotData <- t(rbind(numReads[1:6]/sum(numReads[1:6]),numReadsR1[1:6]/sum(numReadsR1[1:6]),numReadsR2[1:6]/sum(numReadsR2[1:6])))
barplot(barplotData,col=catColors[1:6], beside=T, ylim=c(0,1), names=paste(sprintf("%.1f", as.vector(barplotData)*100), "%"), las=2, space=c(0,.5), cex.axis=1.0, cex.names=1.0, cex.main=1, main="fraction mapQ (both,left,right; >75%,50%-75%,25%-50%,0-25%,0,unmapped)")
abline(h=seq(0,1,0.1), col="gray", lty=3)
barplot(barplotData,col=catColors[1:6], beside=T, ylim=c(0,1), names=paste(sprintf("%.1f", as.vector(barplotData)*100), "%"), las=2, space=c(0,.5), cex.axis=1.0, cex.names=1.0, add=T)

barplotData <- rbind(c(numReadsInPairMapped,numReadsInPairMappedSameChr), c(numReadsInPairMappedNonRed,numReadsInPairMappedSameChrNonRed)) / numReadsInPair
perc <- c(numReadsInPairMappedSameChr / numReadsInPairMapped, numReadsInPairMappedSameChrNonRed / numReadsInPairMappedNonRed)
barplot(barplotData, beside=T, col=barColors[3:6], names=paste(sprintf("%.1f %%", as.vector(barplotData)*100), c("","",sprintf("\n(%.1f %%)", perc*100))), las=2, ylim=c(0,1), space=c(0,.5), cex.axis=1.0, cex.names=1.0, cex.main=1, main="pairs (both map,same chr;nonred)")
abline(h=seq(0,1,0.1), col="gray", lty=3)
barplot(barplotData, beside=T, col=barColors[3:6], names=paste(sprintf("%.1f %%", as.vector(barplotData)*100), c("","",sprintf("\n(%.1f %%)", perc*100))), las=2, ylim=c(0,1), space=c(0,.5), cex.axis=1.0, cex.names=1.0, add=T)


# Real values of bar charts
par(mar=c(0,0,0,0))
data1 <- matrix(c(numTotalReads, numTotalReads1, numTotalReads2, numReadsMapped, numReads1Mapped, numReads2Mapped), 3, 2, byrow=F)
rownames(data1) <- c("Total", "Read1", "Read2")
colnames(data1) <- c("Number", "Mapped")
textplot(data1, cex=0.9, cmar=1, valign="center", mar=c(0,0,1,0))

data2 <- matrix(c(sum(numReads[1:6]),numReads[1:6],sum(numReadsR1[1:6]),numReadsR1[1:6],sum(numReadsR2[1:6]),numReadsR2[1:6]), 3, 7, byrow=T)
rownames(data2) <- c("Total", "Read1", "Read2")
colnames(data2) <- c("Number", "mapQ > 75%", "75% >= mapQ > 50%", "50% >= mapQ > 25%", "25% >= mapQ > 0", "mapQ == 0", "Unmapped")
textplot(data2, cex=0.9, cmar=1, valign="center", mar=c(0,0,1,0))

data3 <- matrix(c(numReadsInPairMapped, numReadsInPairMappedSameChr, numReadsInPairMappedNonRed, numReadsInPairMappedSameChrNonRed, as.integer((1- c(numReadsInPairMappedNonRed/numReadsInPairMapped, numReadsInPairMappedSameChrNonRed/numReadsInPairMappedSameChr)) * 1000)/1000), 3,2, byrow=T)
rownames(data3) <- c("Redun","Non-redun","%redun")
colnames(data3) <- c("Map in pair", "Map same chr") 
textplot(data3, cex=0.9, cmar=1, valign="center", mar=c(0,0,1,0))


## Insert size distribution plot
par(mar=c(3,3,3,1))

# Some default arguments
multimod=FALSE
step=10

#x = c(0,1,2,3,4,5,6,7,8,9,10)

# Some total statistics
ub=boxplot.stats(x)$stats[5]
ub=round((ub + (sqrt(median(x)) * 20)) / 10) * 10
med=median(x)
stats=summary(x)
totalCount=length(x)
sd=sd(x)
# Are there any clusters
x=x[x>=0 & x<=ub]
subCount=length(x)
clus = kmeans(x, 2)
if (abs(clus$centers[1] - clus$centers[2]) > 1000) {
	multimod = TRUE
}
if (ub>2500) { 
	step=10
} else {
	step=5
}

# Plot data
title = "Insert size distribution"
if (multimod) {
	#title = paste("Insert size distribution (", outfile, ")\nPairs in plot:", subCount, "out of", totalCount, "(", round(subCount / totalCount, 4), ")\nOverall median:", round(med,2))
	my_hist=hist(x, breaks=seq(from=0, to=ub+step, by=step), xlim=c(0, ub), main=title, xlab="Insert size", ylab="Frequency", cex.axis=1.0, cex.main=1.0)

	posY = max(my_hist$counts)
	med = median(x[clus$cluster==1])
	mySd = sd(x[clus$cluster==1])
	subCount = length(x[clus$cluster==1])
	abline(v=med, col="blue")
	text(med+10*sqrt(med),posY-2*posY/10, paste("Median: ", med, "\nSd: ", round(mySd,2), "\nPairs: ", subCount, "(", round(subCount / totalCount, 2), ")"), col="blue", pos=4, cex=1.0)

	med = median(x[clus$cluster==2])
	mySd = sd(x[clus$cluster==2])
	subCount = length(x[clus$cluster==2])
	abline(v=med, col="blue")
	text(med+10*sqrt(med),posY-2*posY/10, paste("Median: ", med, "\nSd: ", round(mySd,2), "\nPairs: ", subCount, "(", round(subCount / totalCount, 2), ")"), col="blue", pos=4, cex=1.0)
} else {
	#title = paste("Insert size distribution (", outfile, ")\nPairs in plot:", subCount, "out of", totalCount, "(", round(subCount / totalCount, 4), ")\nOverall median:", round(med,2))
	my_hist=hist(x, breaks=seq(from=0, to=ub+step, by=step), xlim=c(0, ub), main=title, xlab="Insert size", ylab="Frequency", cex.axis=1.0, cex.main=1)

	posY = max(my_hist$counts)
	med = median(x)
	mySd = sd(x)
	subCount = length(x)
	abline(v=med, col="blue")
	text(med+10*sqrt(med),posY-2*posY/10, paste("Median: ", med, "\nSd: ", round(mySd,2)), col="blue", pos=4, cex=1.0)
}


# GC content dependency
numberOfDataPoints <- 5000
gc <- gc[gc$V1 %in% unique(cov$V1),]
gc_sub <- c()
cov_sub <- c()
for (chr in unique(cov$V1)) {
	if (chr %in% c("X", "Y", "M", "chrM.fa", "chrX", "chrX.fa", "chrY", "chrY.fa", "chrM", "chrU", "chr2LHet", "chr2RHet", "chr3LHet", "chr3RHet", "chrXHet", "chrYHet", "U", "2LHet", "2RHet", "3LHet", "3RHet", "XHet", "YHet", "dmel_mitochondrion_genome")) {
		next
	}
	gc_sub <- rbind(gc_sub, gc[gc$V1==chr & gc$V5!=0 & cov$V4>0,])
	cov_sub <- rbind(cov_sub, cov[cov$V1==chr & gc$V5!=0 & cov$V4>0,])
}
set.seed(1)
sel <- sample(1:nrow(gc_sub), min(nrow(gc_sub),numberOfDataPoints), replace=F)
plot(gc_sub[sel, 5], cov_sub[sel, 4], ylim=c(0,median(cov$V4)*3), xlim=c(0.2,0.7), ylab="Coverage", xlab="GC content", pch=20, cex.main=1, main="Coverage vs GC content")
if (length(gc_sub[, 5]) == length(cov_sub[, 4])){
lines(smooth.spline(gc_sub[, 5], cov_sub[, 4], df=6), col="red", lwd=2)
}

# Base qualities
matplot(t(qualR1), type="l", lty=1:6, lwd=1, col="darkgray", ylim=c(0,40), ylab="Avg base quality", xlab="Base position", main="Base quality read 1", cex.axis=1.0, cex.main=1.0)
lines(avgQualR1, lwd=2, col="blue")
abline(h=seq(0,40,5), col="gray", lty=3)
abline(v=seq(0,ncol(samstatData),5), col="gray", lty=3)

matplot(t(qualR2), type="l", lty=1:6, lwd=1, col="darkgray", ylim=c(0,40), ylab="Avg base quality", xlab="Base position", main="Base quality read 2", cex.axis=1.0, cex.main=1.0)
lines(avgQualR2, lwd=2, col="blue")
abline(h=seq(0,40,5), col="gray", lty=3)
abline(v=seq(0,ncol(samstatData),5), col="gray", lty=3)


# Coverage
par(mar=c(0.5,0.5,2.5,0.5))
x <- cov$V4
x <- x + c(x[2:length(x)], rep(0,1))
x <- x + c(x[4:length(x)], rep(0,3))
x <- x + c(x[8:length(x)], rep(0,7))
x <- x + c(x[16:length(x)], rep(0,15))

x[x==0] <- NA
chrBoundaries <- c(which(cov[2:nrow(cov),1]!=cov[1:(nrow(cov)-1),1]), nrow(cov)) / 16
plot(-Inf,-100, xaxt="n", yaxt="n", xlab="", ylab="", xlim=c(0, length(x) /16), ylim=c(0,median(x, na.rm=T)*3), cex.main=1, main="Coverage")
abline(h=median(x, na.rm=T), col="gray", lty=3)
abline(v=c(0, chrBoundaries), col="gray", lty=3)
#plot(x[seq(1,length(x),16)], type="l", xaxt="n", yaxt="n", xlab="", ylab="", ylim=c(0,median(x, na.rm=T)*3))
lines(x[seq(1,length(x),16)])
textPos <- (chrBoundaries + c(0, chrBoundaries[-length(chrBoundaries)])) / 2
text(textPos, median(x, na.rm=T)*2.75, c(cov[cov[2:nrow(cov),1]!=cov[1:(nrow(cov)-1),1],1], cov[nrow(cov),1]) , cex=0.6)



################
#### Page 2 ####
################


layout(matrix(c(rep(1:10, each=3), rep(11:13, each=2), rep(14,6)), 7, 6, byrow = TRUE), heights=c(1,1,1,1,1,2,2))

# Base composition
par(mar=c(1.5,1.5,1.5,0), mgp=c(3,0.1,0),tcl=-0.1)
for (i in 1:5) {
	barplot(as.matrix(avgBaseCompR1[i,]), ylim=c(0,0.4), space=0, width=1, col=nucColors[i], names=1:ncol(samstatData), main=nucleotids[i], cex.axis=0.6, cex=0.6, cex.main=1)
	abline(h=seq(0,0.4,0.1), col="gray", lty=3)
	barplot(as.matrix(avgBaseCompR1[i,]), ylim=c(0,0.4), space=0, width=1, col=nucColors[i], names=1:ncol(samstatData), main=nucleotids[i], cex.axis=0.6, cex=0.6, cex.main=1, add=T)
#	matplot(seq(0.5,ncol(samstatData),1),t(baseCompR1[seq(i,30,5),]), type="l", lty=1:6, lwd=0.5, col="gray", add=T)
	barplot(as.matrix(avgBaseCompR2[i,]), ylim=c(0,0.4), space=0, width=1, col=nucColors[i], names=1:ncol(samstatData), main=nucleotids[i], cex.axis=0.6, cex=0.6, cex.main=1)
	abline(h=seq(0,0.4,0.1), col="gray", lty=3)
	barplot(as.matrix(avgBaseCompR2[i,]), ylim=c(0,0.4), space=0, width=1, col=nucColors[i], names=1:ncol(samstatData), main=nucleotids[i], cex.axis=0.6, cex=0.6, cex.main=1, add=T)
#	matplot(seq(0.5,ncol(samstatData),1),t(baseCompR2[seq(i,30,5),]), type="l", lty=1:6, lwd=0.5, col="gray", add=T)
}


# Mismatches / Insertions / Deletions
if (noAlignInfo) {
	textplot("No mismatch information available", cex=1.8, mar=c(0,0,1,0), fixed.width=F)
	textplot("No insertion information available", cex=1.8, mar=c(0,0,1,0), fixed.width=F)
	textplot("No deletion information available", cex=1.8, mar=c(0,0,1,0), fixed.width=F)
} else {
	barplot(as.matrix(avgMismatches), col=nucColors[1:4], space=0, names=1:ncol(samstatData), main="Mismatches", cex.axis=0.6, cex=0.6, cex.main=1)
	barplot(as.matrix(avgInsertions), col=nucColors[1:4], space=0, names=1:ncol(samstatData), main="Insertions", cex.axis=0.6, cex=0.6, cex.main=1)
	barplot(as.numeric(avgDeletions), col="gray", space=0, names=1:ncol(samstatData), main="Deletions", cex.axis=0.6, cex=0.6, cex.main=1)
}
dev.off()

