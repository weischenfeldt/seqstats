#!/usr/bin/perl

use strict;
use warnings;
use File::Spec;

# Get the main script directory
my $script_dir;
BEGIN {
    my ($volume, $directory, $file) = File::Spec->splitpath(File::Spec->rel2abs($0));
    my @dirs = File::Spec->splitdir($directory); pop(@dirs);
    $script_dir = File::Spec->join(@dirs);
}

print "hello";
print "${script_dir}/libs/";

# Load my custom libray
use lib "${script_dir}/libs/";
use Util;

# Check version
{
    my ($revision, $version, $subversion) = version($^X);
    if (($revision < 5) || ($version<8)) {
	print "Perl version >= 5.10. required.\n";
	exit -1;
    }
}

# Check arguments
if (scalar(@ARGV) != 3) {
    print "Usage: perl $0 <fasta.file> <window_size> <window_offset>\n";
    exit -1;
}

# Main program
my $window_size = $ARGV[1];
my $window_offset = $ARGV[2];
if ($window_offset > $window_size) { $window_offset = $window_size; }
my $chr = "";
my $pos = 0;
my $seq = "";
my %acgt;
open(FASTA, $ARGV[0]);
while (<FASTA>) {
    chomp($_);
    if (substr($_, 0, 1) eq '>') { 
	if (length($seq)) {
	    gcContent(\$seq, \%acgt);
	    print $chr ."\t". $pos ."\t". ($pos + length($seq)) ."\t". $acgt{'N'} ."\t". $acgt{'gc'} ."\n";
	    #print $chr ."\t". $seq ."\t". $acgt{'N'} ."\t". $acgt{'gc'} ."\n";
	}
	$chr = substr($_, 1); 
	$pos = 0;
	$seq = "";
    } else {
	my $start_char = 0;
	my $chunk = $window_size - length($seq);
	my $rem_char = length($_);
	while ($rem_char >= $chunk) {
	    $seq .= substr($_, $start_char, $chunk);
	    gcContent(\$seq, \%acgt);
	    print $chr ."\t". $pos ."\t". ($pos + length($seq)) ."\t". $acgt{'N'} ."\t". $acgt{'gc'} ."\n";
	    #print $chr ."\t". $seq ."\t". $acgt{'N'} ."\t". $acgt{'gc'} ."\n";
	    if ($window_offset == length($seq)) { $seq=""; }
	    else { $seq = substr($seq, $window_offset); }
	    $pos += $window_offset;
	    $start_char += $chunk;
	    $rem_char -= $chunk;
	    $chunk = $window_size - length($seq);
	}
	$seq .= substr($_, $start_char, $rem_char);
    }
}
if (length($seq)) {
    gcContent(\$seq, \%acgt);
    print $chr ."\t". $pos ."\t". ($pos + length($seq)) ."\t". $acgt{'N'} ."\t". $acgt{'gc'} ."\n";
}
close(FASTA);
