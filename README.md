# Seqstats

QC of BAM files

### Environment module ###

The tool is installed as a environment module in computerome.
To use the script installed in the module type the command:

```

module add tools R/3.2.1 samtools/1.2
module add seqstats

```

The environment module can handle various version of the same
tool.

The path of the modulefiles are installed:

```
/home/projects/cu_10027/apps/modulefiles/seqstats
```

The path of the binary and the run_seqstats script:

```
/home/projects/cu_10027/apps/software/seqstats
```

The content of the bin folder in the env module:

```
ls /home/projects/cu_10027/apps/software/seqstats/0.5.6/bin -hls
total 15M
4.0M -rwxrwx--- 1 s093829 cu_10027 3.2M Aug 16 20:12 cov
4.0M -rwxrwx--- 1 s093829 cu_10027 3.2M Aug 16 20:12 cov_v0.5.6_parallel_linux_x86_64bit
2.3M -rwxrwx--- 1 s093829 cu_10027 1.9M Aug 16 20:12 cov_v1
 66K -rw-rw---- 1 s093829 cu_10027  15K Aug 16 20:12 createSeqstatsPdf.R
 66K -rw-rw---- 1 s093829 cu_10027  15K Aug 16 20:12 createSeqstatsv2Pdf.R
 34K -rwxrwx--- 1 s093829 cu_10027 2.1K Aug 17 15:37 gcContentWindow.pl
2.0M -rwxrwx--- 1 s093829 cu_10027 1.6M Aug 16 20:12 insertSize
 34K -rwxrwx--- 1 s093829 cu_10027 6.4K Aug 18 10:29 run_seqstats
418K -rwxrwx--- 1 s093829 cu_10027  98K Aug 16 20:12 samstatM
 34K -rwxrwx--- 1 s093829 cu_10027 3.0K Aug 16 21:07 seqstats_crome.sh
2.0M -rwxrwx--- 1 s093829 cu_10027 1.6M Aug 16 20:12 stats
```

### Pipeline in computerome ###

The `seqstats` environment module is also included into the
`pype` command in computerome.

In order to use the seqstats tools in the `pype` command
there is no need to previously load the `seqstats` module:

```
module add pype
```

To use the tool:

```
pype snippets seqstats
```
